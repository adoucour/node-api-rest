var express = require("express");
const productController = require("../controllers/productController");
const guard = require("../middlewares/guard");
const productImageUpdate = require("../middlewares/multer.config");
var router = express.Router();

router.get("/", guard, productController.list);

router.get("/:id", guard, productController.show);

router.post("/", productImageUpdate, guard, productController.create);

router.put("/:id", productImageUpdate, guard, productController.update);

router.delete("/:id", guard, productController.delete);

module.exports = router;
