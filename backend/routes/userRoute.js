var express = require("express");
const userController = require("../controllers/userController");
var router = express.Router();

router.post("/signup", userController.signup);

router.post("/login", userController.login);

module.exports = router;
