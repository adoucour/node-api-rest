const productModel = require("../models/productModel");
const fs = require("fs");

/**
 * productController.js
 *
 * @description :: Server-side logic for managing products
 */

module.exports = {
  // Get all products
  list: (req, res) => {
    productModel.find((err, products) => {
      if (err) {
        return res.status(500).json({
          status: 500,
          message: "Impossible d'avoir la liste des produits",
        });
      }
      return res.status(200).json({
        status: 200,
        data: products,
      });
    });
  },

  // Get product by id
  show: (req, res) => {
    const id = req.params.id;
    productModel.findOne({ _id: id }, (err, product) => {
      if (err) {
        return res.status(500).json({
          status: 500,
          message: "Impossible d'avoir ce produit",
        });
      }
      if (!product) {
        return res.status(404).json({
          status: 404,
          message: "Ce produit n'éxiste pas",
        });
      }
      return res.status(200).json({
        status: 200,
        data: product,
      });
    });
  },
  // create product
  create: (req, res) => {
    if (!req.file) {
      return res.status(500).json({
        status: 500,
        message: "L'image est requise",
      });
    }

    const productFromClient = JSON.parse(req.body.product);
    delete productFromClient._id;

    var product = new productModel({
      ...productFromClient,
      image: `${req.protocol}://${req.get("host")}/images/products/${
        req.file.filename
      }`,
    });

    product.save((err, productCreated) => {
      if (err) {
        return res.status(500).json({
          status: 500,
          message: "Impossible de créer ce produit",
        });
      }
      return res.status(201).json({
        status: 201,
        message: "Création du produit",
      });
    });
  },
  // product update
  update: (req, res) => {
    const id = req.params.id;
    let productUpdateFromClient = JSON.parse(req.body.product);

    if (req.file) {
      productUpdateFromClient.image = `${req.protocol}://${req.get(
        "host"
      )}/images/products/${req.file.filename}`;
      productModel.findOne(
        { _id: id },
        { image: true },
        (err, productUpdate) => {
          if (err) {
            console.log(err);
            return;
          }
          const oldImage = productUpdate.image.split("/products/")[1];
          productUpdate.image = `${req.protocol}://${req.get(
            "host"
          )}/images/products/${req.file.filename}`;
          fs.unlink(`public/images/products/${oldImage}`, (error) => {
            if (error) {
              console.log(error.message);
            }
          });
        }
      );
    }

    productModel.updateOne(
      { _id: id },
      { ...productUpdateFromClient, _id: id },
      (err, data) => {
        if (err) {
          return res.status(500).json({
            status: 500,
            message: "Erreur lors de la mise à jour du produit !",
            erreur: err,
          });
        }
        return res.status(200).json({
          status: 200,
          message: "Produit mise à jour !",
        });
      }
    );

    // productModel.findOne({ _id: id }, (err, productUpdate) => {
    //   if (err) {
    //     return res.status(500).json({
    //       status: 500,
    //       message: "Impossible de mettre à jour ce produit",
    //     });
    //   }
    //   if (!productUpdate) {
    //     return res.status(404).json({
    //       status: 404,
    //       message: "Produit introuvable",
    //     });
    //   }
    //   productUpdate.name = req.body.name ? req.body.name : productUpdate.name;
    //   productUpdate.description = req.body.description
    //     ? req.body.description
    //     : productUpdate.description;
    //   productUpdate.price = req.body.price
    //     ? req.body.price
    //     : productUpdate.price;
    //   productUpdate.stock = req.body.stock
    //     ? req.body.stock
    //     : productUpdate.stock;
    //   productUpdate.userId = req.body.userId
    //     ? req.body.userId
    //     : productUpdate.userId;
    //   productUpdate.createdAt = req.body.createdAt
    //     ? req.body.createdAt
    //     : productUpdate.createdAt;

    //   if (req.file) {
    //     const oldImage = productUpdate.image.split("/products/")[1];
    //     productUpdate.image = `${req.protocol}://${req.get(
    //       "host"
    //     )}/images/products/${req.file.filename}`;
    //     fs.unlink(`public/images/products/${oldImage}`, (error) => {
    //       if (error) {
    //         console.log(error.message);
    //       }
    //     });
    //   }

    //   productUpdate.save((error, product) => {
    //     if (error) {
    //       return res.status(500).json({
    //         status: 500,
    //         message: "Erreur lors de la mise à jour ce produit",
    //       });
    //     }
    //     return res.status(200).json({
    //       status: 200,
    //       message: "Produit mise à jour !",
    //     });
    //   });
    // });
  },
  delete: (req, res) => {
    const id = req.params.id;
    productModel.findByIdAndRemove(id, (err, productDelete) => {
      if (err) {
        return res.status(500).json({
          status: 500,
          message: "Impossible de supprimer ce produit",
        });
      }
      if (!productDelete) {
        return res.status(404).json({
          status: 404,
          message: "Produit introuvable",
        });
      }

      const fileImage = productDelete.image.split("/products/")[1];
      fs.unlink(`public/images/products/${fileImage}`, (error) => {
        if (error) {
          console.log(error.message);
        }
      });

      return res.status(204).json({
        status: 204,
        message: "Produit supprimé !",
      });
    });
  },
};
