const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/userModel");

/**
 * userController.js
 *
 * @description :: Server-side logic for managing users
 */

module.exports = {
  signup: (req, res) => {
    bcrypt.hash(req.body.password, 10, (err, hash) => {
      if (err) {
        return res.status(500).json({
          status: 500,
          message: err.message,
        });
      }
      const newUser = User({
        email: req.body.email,
        password: hash,
      });

      newUser.save((err, user) => {
        if (err) {
          return res.status(400).json({
            status: 400,
            message: err.message,
          });
        }
        return res.status(201).json({
          status: 201,
          message: "L'utilisateur a bien été créé",
        });
      });
    });
  },
  login: (req, res) => {
    User.findOne({ email: req.body.email }, (err, user) => {
      if (err) {
        return res.status(500).json({
          status: 500,
          message: err.message,
        });
      }
      if (!user) {
        return res.status(404).json({
          status: 404,
          message: "Utilisateur introuvable !",
        });
      }
      bcrypt.compare(req.body.password, user.password, (err, found) => {
        if (err) {
          return res.status(500).json({
            status: 500,
            message: err.message,
          });
        }
        if (!found) {
          return res.status(401).json({
            status: 401,
            message: "Mot de passe incorrect",
          });
        }

        return res.status(200).json({
          userId: user._id,
          token: jwt.sign({ userId: user._id }, process.env.TOKEN_SECRET, {
            expiresIn: "24h",
          }),
        });
      });
    });
  },
};
