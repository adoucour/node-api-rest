import { Injectable } from '@angular/core';
import { Cart } from '../models/cart';
import { Product } from '../models/product';
import { Item } from '../models/item';
import { environment } from '../../environments/environment';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  cart: Cart = new Cart();
  cart$ = new Subject<Cart>();
  tva: number = environment.tva / 100;

  constructor() {}

  emitCart() {
    this.cart$.next(this.cart);
  }

  addToCart(product: Product) {
    const itemAdd = this.cart.items.find(
      (item) => item.product._id === product._id
    );

    if (itemAdd) {
      itemAdd.quantity++;
    } else {
      const newItem = new Item();
      newItem.product = product;
      newItem.quantity = 1;
      this.cart.items.push(newItem);
    }
    this.updateCart();
  }

  updateCart() {
    this.cart.resume = { quantity: 0, coastHT: 0, coastTVA: 0, coastTTC: 0 };

    this.cart.items.forEach((item) => {
      this.cart.resume.quantity += item.quantity;
      this.cart.resume.coastHT += item.quantity * item.product.price;
      this.cart.resume.coastTVA += this.cart.resume.coastHT * this.tva;
      this.cart.resume.coastTTC += this.cart.resume.coastHT * (1 + this.tva);
    });
    this.emitCart();
  }

  removeOne(product: Product) {
    const itemDelete = this.cart.items.find(
      (item) => item.product._id === product._id
    );

    if (itemDelete) {
      if (itemDelete.quantity > 1) {
        itemDelete.quantity--;
      } else {
        const index = this.cart.items.indexOf(itemDelete);
        this.cart.items.splice(index, 1);
      }
      this.updateCart();
    }
  }

  removeMany(product: Product) {
    const itemDelete = this.cart.items.find(
      (item) => item.product._id === product._id
    );

    if (itemDelete) {
      const index = this.cart.items.indexOf(itemDelete);
      this.cart.items.splice(index, 1);
      this.updateCart();
    }
  }
}
