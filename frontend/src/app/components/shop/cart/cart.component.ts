import { Component, OnInit } from '@angular/core';
import { Cart } from '../../../models/cart';
import { CartService } from '../../../services/cart.service';
import { environment } from '../../../../environments/environment';
import { Product } from 'src/app/models/product';

@Component({
  selector: 'node-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {
  cart: Cart;
  items;

  tva = environment.tva;
  constructor(private cartService: CartService) {}

  ngOnInit(): void {
    this.cartService.cart$.subscribe((cartData: Cart) => {
      this.cart = cartData;
      this.items = cartData.items;
    });

    this.cartService.emitCart();
  }

  addToCart(product: Product) {
    this.cartService.addToCart(product);
  }

  removeOne(product: Product) {
    this.cartService.removeOne(product);
  }

  removeMany(product: Product) {
    this.cartService.removeMany(product);
  }
}
