import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Product } from 'src/app/models/product';
import { ProductService } from '../../services/product.service';
import { AuthService } from '../../services/auth.service';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'node-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css'],
})
export class ShopComponent implements OnInit, OnDestroy {
  products: Product[] = [];
  productSub: Subscription;
  userId: string = '';
  loading: boolean = false;

  constructor(
    private productService: ProductService,
    private auth: AuthService,
    private cartService: CartService
  ) {}

  ngOnInit(): void {
    this.userId = this.auth.userId;

    this.productSub = this.productService.products$.subscribe(
      (products: Product[]) => {
        this.loading = true;
        this.products = products;
      },
      (error) => {
        this.loading = false;
        console.log(error);
      }
    );
    this.productService.getProducts();
  }

  ngOnDestroy(): void {
    this.productSub.unsubscribe();
  }

  addToCart(product: Product) {
    console.log(this.cartService.cart);

    this.cartService.addToCart(product);
  }
}
