import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Product } from 'src/app/models/product';
import { AuthService } from '../../../services/auth.service';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'node-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css'],
})
export class EditProductComponent implements OnInit {
  productForm: FormGroup;
  errorMessage: string = '';
  imagePreview: string = '';
  loading: boolean = false;
  userId: string = '';
  product: Product = new Product();

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private productService: ProductService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.userId = this.auth.userId;
    this.loading = true;

    this.route.params.subscribe((params: Params) => {
      this.productService
        .getProductsById(params.id)
        .then((product: Product) => {
          this.product = product;
          if (this.product.userId !== this.userId) {
            this.router.navigate(['/not-found']);
            return;
          }

          this.productForm = this.formBuilder.group({
            name: [product.name, Validators.required],
            description: [product.description, Validators.required],
            stock: [product.stock, Validators.required],
            price: [product.price / 100, Validators.required],
            image: [product.image, Validators.required],
          });
          this.imagePreview = product.image;
          this.loading = false;
        })
        .catch((error) => {
          console.log(error.message);
          return this.router.navigate(['/shop']);
        });
    });
  }

  onSubmit() {
    this.loading = true;
    const product = new Product();

    if (this.product.userId !== this.userId) {
      this.router.navigate(['/not-found']);
      return;
    }

    product._id = this.product._id;
    product.name = this.productForm.get('name').value;
    product.description = this.productForm.get('description').value;
    product.price = this.productForm.get('price').value * 100;
    product.stock = this.productForm.get('stock').value;
    product.image = '';
    product.userId = this.product.userId;

    this.productService
      .updateProduct(product._id, product, this.productForm.get('image').value)
      .then(() => {
        this.productForm.reset();
        this.loading = false;
        this.router.navigate(['/shop']);
      })
      .catch((error) => {
        this.loading = false;
        this.errorMessage = error.message;
      });
  }

  onImagePick(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.productForm.get('image').patchValue(file);
    this.productForm.get('image').updateValueAndValidity();

    const reader = new FileReader();
    reader.onload = () => {
      if (this.productForm.get('image').valid) {
        this.imagePreview = reader.result as string;
      } else {
        this.imagePreview = '';
      }
    };
    reader.readAsDataURL(file);
  }
}
