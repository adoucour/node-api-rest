import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Cart } from '../../../models/cart';
import { CartService } from '../../../services/cart.service';

@Component({
  selector: 'node-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  isAuth: boolean = false;
  resume;

  constructor(private auth: AuthService, private cartService: CartService) {}

  ngOnInit(): void {
    this.cartService.cart$.subscribe(
      (cart: Cart) => {
        this.resume = cart.resume;
      },
      (error) => {
        console.log(error);
      }
    );

    this.cartService.emitCart();

    this.auth.isAuth$.subscribe((bool: boolean) => {
      this.isAuth = bool;
    });
  }

  logout() {
    this.auth.logout();
  }
}
