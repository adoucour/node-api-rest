import { Item } from './item';
export class Cart {
  items: Item[] = [];
  resume: {
    quantity: number;
    coastHT: number;
    coastTVA: number;
    coastTTC: number;
  };
}
